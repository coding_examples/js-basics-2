/*
 * Setup for Node environment
 * - Execute "npm install node-fetch" to install fetch API library
 * - Import fetch using "const fetch = require('node-fetch')"
 */
const fetch = require("node-fetch");

// What does "https://api.github.com/users" return? - Try visiting from browser

// Fetch Response object
fetch(`https://api.github.com/users`).then((res) => console.log(res));

// Check if response was successful - status codes in [200, 299]
fetch(`https://api.github.com/users`).then((res) => console.log(res.ok));

/*
 * Parsing response data in JSON format to JS object
 */
// Using .then().catch()
// fetch(`https://api.github.com/users`)
//   .then((res) => res.json())
//   .then((data) => console.log(data))
//   .catch((error) => console.log(error));

// Using async-await
async function main() {
  try {
    let res = await fetch(`https://api.github.com/users`);
    let data = await res.json();

    // print *array* of user *objects* returned
    console.log(data);
  } catch (err) {
    console.log(err);
  }
}

main();
