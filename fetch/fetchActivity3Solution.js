/*
 * Setup for Node environment
 * - Execute "npm install node-fetch" to install fetch API library
 * - Import fetch using "const fetch = require('node-fetch')"
 */
const fetch = require("node-fetch");

/*
 * Create an array of Github usernames returned from Github API whose “id” value is less than 10
 * https://api.github.com/users
 */
async function getGithubUsernamesFiltered() {
  let res = await fetch("https://api.github.com/users");
  let data = await res.json();

  let filteredUserObjects = data.filter((userObject) => userObject.id < 10);
  let usernames = filteredUserObjects.map((userObject) => userObject.login);

  return usernames;
}

async function main() {
  let usernames = await getGithubUsernamesFiltered();
  console.log(usernames);
}

main();
