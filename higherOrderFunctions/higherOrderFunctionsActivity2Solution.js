let runsScored = [20, 100, 53, 44, 21];
let newArray = [];

for (let i = 0; i < runsScored.length; i++) {
  let currScore = runsScored[i];

  if (currScore >= 50) {
    newArray.push(currScore);
  } else {
    newArray.push(0);
  }
}

console.log(newArray);

// re-write logic using higher order function map or filter appropriately
let newArrayHOC = runsScored.map(currScore => {
      if (currScore >= 50) {
        return currScore;
      } else {
        return 0;
      }
})

console.log(newArrayHOC)