/*
 * Creating a new object
 */

// objects defined using literal syntax - {}
let scorecard = {
  runs: 329,
  wicketsLost: 9,
  team: "India",
};

console.log(scorecard);

// objects defined using Object constructor
// let scorecardConstructor = new Object();

// scorecardConstructor.runs = 329;
// scorecardConstructor.wicketsLost = 9;
// scorecardConstructor.team = "India";

// console.log(scorecardConstructor);

/*
 * Reading and updating object properties
 */

// Access value of properties
// let runs = scorecard.runs;
// console.log(runs);

// runs = scorecard["runs"];
// console.log(runs);

// update value of existing property
// scorecard.runs = 400;
// console.log(scorecard.runs);

// add a new property
// scorecard.innnings = 2;
// console.log(scorecard);

// delete a property
// delete scorecard.innnings;
// console.log(scorecard);

/*
 * "this" keyword and functions as values
 */
// let scorecard = {
//   runs: 329,
//   wicketsLost: 9,
//   team: "India",
//   summary: this.team + " scored " + this.runs + " runs!",
//   chant: function () {
//     return "Go India, go!";
//   },
// };

// console.log(scorecard);
// console.log(scorecard.summary);
// console.log(scorecard.chant());