/*
 * Handle below promises using async-await syntax such that
 * - If “promise1” is resolved, print resolved value and print “promise2” output as well
 * - If “promise1” is rejected, print error but don’t get “promise2” output
 */
async function asyncFn1() {
  return "asyncFn1";
  //   throw "asyncFn1Error";
}

async function asyncFn2() {
  return "asyncFn2";
}

async function main() {
  try {
    let promise1 = await asyncFn1();
    console.log(promise1);

    let promise2 = await asyncFn2();
    console.log(promise2);
  } catch (err) {
    console.log(err);
  }
}

main();
