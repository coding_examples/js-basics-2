/*
 * Handle below promises such that
 * - If “promise1” is resolved, print resolved value and print “promise2” output as well
 * - If “promise1” is rejected, print error but don’t get “promise2” output
 */
function asyncFn1() {
  return Promise.resolve("asyncFn1");
  //   return Promise.reject("asyncFn1Error");
}

function asyncFn2() {
  return Promise.resolve("asyncFn2");
}

let promise1 = asyncFn1();
let promise2 = asyncFn2();
