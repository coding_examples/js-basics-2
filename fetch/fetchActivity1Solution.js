/*
 * Setup for Node environment
 * - Execute "npm install node-fetch" to install fetch API library
 * - Import fetch using "const fetch = require('node-fetch')"
 */
const fetch = require("node-fetch");

/*
 * Print count of user objects returned from Github API - https://api.github.com/users
 */
async function getGithubUserCount() {
  let res = await fetch("https://api.github.com/users");
  let data = await res.json();

  let count = data.length;

  return count;
}

async function main() {
  let count = await getGithubUserCount();
  console.log(count);
}

main();
