/*
 * Create a “Person” object using the “literal” syntax with
 * - 4 properties - name, age, gender, address
 * - 1 function - details() : prints “name” and “age” of the person
 */

// Approach 1 - using "this" keyword
let person = {
  name: "Kitty",
  age: 5,
  gender: "femail",
  address: "Bengaluru",
  details: function () {
    let msg =
      "Hi, my name is " + this.name + " and I'm " + this.age + " years old!";
    console.log(msg);
    // let name = "Tom";
    // let msg = "Hi, my name is " + name + " and I'm " + this.age + " years old!";
  },
};

person.details(); // should print "Hi, my name is <name> and I'm <age> years old!"

// Approach 2
// let person = {
//   name: "Kitty",
//   age: 5,
//   gender: "femail",
//   address: "Bengaluru",
//   details: function (name, age) {
//     let msg = "Hi, my name is " + name + " and I'm " + age + " years old!";
//     console.log(msg);
//   },
// };

// person.details(person.name, person.age); // need to pass data explicitly
