/*
 * Create a “Person” object using the “literal” syntax with 
 * - 4 properties - name, age, gender, address
 * - 1 function - details() : prints “name” and “age” of the person
 */
let person = {};

person.details(); // should print "Hi, my name is <name> and I'm <age> years old!"