/*
 * for-loops vs map
 */

// scorecard of Kholi's batting performance over a 5-match ODI series
let runsScored = [20, 100, 53, 44, 21];

// calculate match income -> 10 x runs scored
let incomeArray = [];

for (let i = 0; i < runsScored.length; i++) {
  incomeArray.push(runsScored[i] * 10);
}

console.log(incomeArray);

// calculate match income -> 10 x runs scored
// incomeArray = runsScored.map(currScore => currScore * 10); // callback function returns modified element
// console.log(incomeArray);

/*
 * for-loops vs filter
 */
// let runsScored = [20, 100, 53, 44, 21];

// find match scores with atleast a 50
// let fiftyScores = [];

// for (let i = 0; i < runsScored.length; i++) {
//   let currScore = runsScored[i];

//   if (currScore >= 50) {
//     fiftyScores.push(currScore);
//   }
// }

// console.log(fiftyScores);

// find match scores with atleast a 50
// fiftyScores = runsScored.filter(currScore => currScore >= 50); // callback function returns if element satisfied condition

// same as
// fiftyScores = runsScored.filter((currScore) => {
//   return currScore >= 50;
// });

// same as
// fiftyScores = runsScored.filter(function (currScore) {
//   return currScore >= 50;
// });
