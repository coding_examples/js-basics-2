/* 
 * Creating arrays
 */
let scores = [20, 100, 53, 44, 21];
console.log(scores)
console.log(scores.length)

/*
 * Indexing arrays
 * 20 -> 0th index
 * 100 -> 1st index
 * 21 -> 4th index
 */
// console.log(scores[0]);  // 1st element
// console.log(scores[3]) // 4th element

/*
 * Looping over arrays
 *
 * 1st match income -> 1 x runs
 * 2nd match income -> 2 x runs
 * 3rd match income -> 3 x runs
 */
// for (let i = 0; i < scores.length; i++) {
//   console.log(scores[i] * (i + 1)); // 1st match -> Index = 0
// }

/*
 * Manipulating arrays
 */
// let scores = [20, 100, 53, 44, 21];

// updating elements
// scores[0] = 21; // replaces 20
// scores[10] = 0;  // throws error as index 11 isn't availabel

// console.log(scores)

// adding new element to the end of array
// scores.push(0);
// console.log(scores);

// removing element from the end of the array
// scores.pop();
// console.log(scores);