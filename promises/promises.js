/*
 * synchronous vs asynchronous execution of code
 * Note: setTimeout isn't a Promise neither does it return one
 */

// Synchronous loading of google.com webpage - Takes 7s to display web page to user
console.log("[00:00] - Request received to load google.com");
console.log("[00:01] - HTML content requested from google.com server");
console.log("[00:02] - HTML content received from google.com server");
console.log("[00:03] - Images requested from google.com server");
console.log("[00:04] - Waiting for images from google.com");
console.log("[00:05] - Waiting for images from google.com");
console.log("[00:06] - Images received");
console.log("[00:07] - Webpage displayed");
console.log("[00:08] - Images displayed");
console.log();

// Asynchronous loading of google.com webpage - Takes 3s to display web page to user
console.log("[00:00] - Request received to load google.com");
console.log("[00:01] - HTML content requested from google.com server");
console.log("[00:02] - HTML content received from google.com server");
console.log("[00:03] - Webpage displayed");
setTimeout(() => {
    console.log("Images requested from google.com server");
    console.log("Waiting for images from google.com");
    console.log("Waiting for images from google.com");
    console.log("Images received");
    console.log("Images displayed");
}, 2000)

/*
 * Creating Promise
 * Note:
 *  - Better to demo in Chrome Developer Tools
 */
// let promise = new Promise(function (resolve, reject) {
//   // long running operation
//   // when success -> resolve
//   // when failure -> reject

//   let longRunning = function () {
//     let msg = "Message";

//     setTimeout(() => {
//         resolve(msg)
//     }, 1000); // takes ~1s to complete
//   };

//   console.log("Start execution");
//   longRunning(); // don't wait for this function to complete
//   console.log("Continue execution");
// });

// console.log(promise);

// .then(), .catch() again are asynchronous
// console.log("Before");
// promise
//   .then(function (msg) {
//     console.log("Success: ", msg);
//   })
//   .catch(function (err) {
//     console.log("Failure: ", err);
//   });
// console.log("After");

/*
 * Chaining Promises
 * Note: Better to demo in Chrome Developer Tools
 */
// let promise1 = new Promise(function (resolve, reject) {
//   let longRunning1 = function () {
//     let msg = "Promise1";

//     setTimeout(() => {
//         resolve(msg)
//     }, 2000); // takes ~2s to complete
//   };

//   console.log("Start execution");
//   longRunning1(); // don't wait for this function to complete
//   console.log("Continue execution");
// });

// let promise2 = new Promise(function (resolve, reject) {
//   let longRunning2 = function () {
//     let msg = "Promise2";

//     setTimeout(() => {
//         resolve(msg)
//     }, 1000); // takes ~1s to complete
//   };

//   longRunning2();
// });

// Case 1 - fetching promise2 output only after promise1 is fulfilled
// promise1
//   .then(function (msg) {
//     console.log("Promise 1 Success: ", msg);
//     return promise2;
//   })
//   .then(function (msg) {
//     console.log("Promise 2 Success: ", msg);
//   })
//   .catch(function (err) {
//     console.log("Failure: ", err);
//   });

// Case 2 - fetching promise2 output independent of promise1
// promise1
//   .then(function (msg) {
//     console.log("Promise 1 Success: ", msg);
//   })
//   .catch(function (err) {
//     console.log("Failure: ", err);
//   });

// promise2
//   .then(function (msg) {
//     console.log("Promise 2 Success: ", msg);
//   })
//   .catch(function (err) {
//     console.log("Failure: ", err);
//   });

/*
 * Promise shorthand
 */

// Promise - normal syntax
// function longRunning() {
//   if (true) {
//     return new Promise((resolve, reject) => {
//       resolve("longRunning");
//     });
//   }
// }
// let promise = longRunning();

// promise
//   .then((msg) => console.log("Success: ", msg))
//   .catch((err) => console.log("Error: ", err));

// Promise - shorthand
// function longRunning1() {
//   if (true) {
//     return Promise.resolve("longRunning1");
//   }
// }

// let promiseShorthand = longRunning1();

// promiseShorthand
//   .then((msg) => console.log("Success: ", msg))
//   .catch((err) => console.log("Error: ", err));
