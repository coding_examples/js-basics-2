/*
 * An array of names, “names” is given
 * Create a new array of names from “names” which starts with an “A”
 */

let names = ["Anil", "Dhoni", "Kohli", "Aaron", "Ajinkya"];

// store names starting with "A"
let namesStartingWithA;

console.log(namesStartingWithA); // should print [ 'Anil', 'Aaron', 'Ajinkya' ]
