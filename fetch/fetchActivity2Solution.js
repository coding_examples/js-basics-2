/*
 * Setup for Node environment
 * - Execute "npm install node-fetch" to install fetch API library
 * - Import fetch using "const fetch = require('node-fetch')"
 */
const fetch = require("node-fetch");

/*
 * Create an array of Github usernames returned from Github API - https://api.github.com/users
 */
async function getGithubUsernames() {
  let res = await fetch("https://api.github.com/users");
  let data = await res.json();

  let usernames = data.map((userObject) => userObject.login);

  return usernames;
}

async function main() {
  let usernames = await getGithubUsernames();
  console.log(usernames);
}

main();
