/*
 * Setup for Node environment
 * - Execute "npm install node-fetch" to install fetch API library
 * - Import fetch using "const fetch = require('node-fetch')"
 */
const fetch = require("node-fetch");

/*
 * Create an array of Github usernames returned from Github API whose “id” value is less than 10
 * https://api.github.com/users
 */
