/*
 * Promises with Promise
 */
let promise = Promise.resolve("longRunning");

promise
  .then((msg) => console.log("Success: ", msg))
  .catch((err) => console.log("Error: ", err));

/*
 * Async functions always return a promise
 */
// async function longRunning() {
//   return "longRunning"; // same as Promise.resolve(msg)
//   // throw "longRunning"; // same as Promise.reject(msg)
// }

// let promiseAsync = longRunning(); // outputs a Promise object

// console.log("hi")
// promiseAsync
//   .then((msg) => console.log("Success: ", msg))
//   .catch((err) => console.log("Error: ", err));
// console.log("bye");

/*
 * Using await - can only be used from within an async function
 *
 * - removes usage of .then()
 * - use try-catch for rejections i.e, instead of .catch()
 * - allows storing Promise fulfilled value to a variable
 */

// async function longRunning() {
//   return "longRunning";
// }

// async function main() {
//   console.log("hi");
//   try {
//     let msg = await longRunning();
//     console.log(msg);
//     console.log("bye");
//   } catch (err) {
//     console.log(err);
//   }
// }

// main();

/* -> Promise alternative
  let promise = longRunning();

  console.log("hi");
  promise
  .then((msg) => {
    console.log(msg);
    console.log("bye")
  }).catch((err) => {
    console.log(err);
  });
*/

/*
 * Getting Promises to wait for each other
 */
// async function promise1() {
//   return "Promise1";
// }

// async function promise2() {
//   return "Promise2";
// }

// async function main() {
//   let msg1 = await promise1();
//   let msg2 = await promise2(); // line is executed only after promise on prev line is fulfilled

//   console.log(msg1);
//   console.log(msg2);
// }

// main();
