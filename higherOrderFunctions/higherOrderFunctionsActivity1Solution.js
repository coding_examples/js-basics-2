/*
 * An array of names, “names” is given
 * Create a new array of names from “names” which starts with an “A”
 */

let names = ["Anil", "Dhoni", "Kohli", "Aaron", "Ajinkya"];

// store names starting with "A"
let namesStartingWithA = names.filter((name) => name[0] === "A");

console.log(namesStartingWithA);
